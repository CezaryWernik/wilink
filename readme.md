WiLink Google Chrome extensions
================
This repository contains an extension for Google Chrome.
This supplement is designed for Students WI ZUT START SUMMER 2016.
In the future, I plan to expand the project with the ability to configure.
Available for installation only in developer mode.
____

## Requirements
* Google Chrome
* Internet connection ;)

## Installation
* Download and save to directory WiLink this repository, eg. by cloning directly from the repo:
~~~~
  $ Git clone https: //CezaryWernik@bitbucket.org/CezaryWernik/wilink.git
  Cloning into 'WiLink' ...
  remote: Counting objects: 30, done.
  remote: Compressing objects: 100% (29/29), done.
  remote: Total 30 (delta 3), reused 0 (delta 0)
  Unpacking objects: 100% (30/30), done.
  Checking connectivity ... done.
~~~~

* In the browser address bar, type [chrome://extensions](chrome://extensions)
should show the panel extensions similar to the image below.
If you are not in developer mode then turn now:
![Image](readme/1.png)

* Select "Load unpacked extension":
![Image](readme/2.png)

* Find and select the folder WiLink:
![Image](readme/3.png)

* You should see a similar report to the image below:
![Image](readme/4.png)

* In the upper right corner of the browser has been added extension WiLink:
![Image](readme/5.png)

* Done!
